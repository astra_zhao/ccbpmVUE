import Vue from 'vue'
import VueRouter from 'vue-router'
import home from '../wf/appclassic/home.vue'
import login from '../wf/appclassic/login.vue'
import myflow from '../wf/myFlow.vue'
import MyFlowGener from '../wf/myFlowGener.vue'
import start from '../wf/start.vue' //发起
import todolist from '../wf/todolist.vue' //待办
import runing from '../wf/runing.vue' // 在途
import complete from '../wf/complete.vue' //已完成
import demo from '../demo/demo.vue' //已完成

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: home,
    children: [
      {
        path: '/start',
        name: 'start',
        component: start,
      },
      {
        path: '/todolist',
        name: 'todolist',
        component: todolist,
      },
      {
        path: '/runing',
        name: 'runing',
        component: runing,
      },
      {
        path: '/complete',
        name: 'complete',
        component: complete,
      },
      {
        path:'/myflow',
        name: 'myflow',
        component: myflow
      },
      {
        path:'/MyFlowGener',
        name: 'MyFlowGener',
        component: MyFlowGener
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: login
  },
  
  {
    path:'/demo',
    name: 'demo',
    component: demo
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
