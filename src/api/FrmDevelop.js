import $ from 'jquery'
import { HttpHandler} from './Gener.js';
import {webPath} from './config.js';
import {InitDDLOperation,ConvertDefVal} from './tools.js';

import datetimepicker from '../datetimepicker/js/bootstrap-datetimepicker.js';
import zhCN from '../datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js';


/**
 * 获取开发者表单内容
 */
var _this; //表单组件
var currentURL = window.document.location.href;
function GenerDevelopFrm(obj){
    _this = obj;
    //获取开发者表单的内容
    var filename = webPath + "/DataUser/CCForm/HtmlTemplateFile/" + _this.flowData.Sys_MapData[0].No + ".htm";
    var htmlobj = $.ajax({ url: filename, async: false });
    var htmlContent = "";
    //warning 暂时设置
    htmlobj.status = 404;
    if (htmlobj.status == 404) {
        //数据库中查找
        var handler = new HttpHandler("BP.WF.HttpHandler.WF_Admin_DevelopDesigner");
        handler.AddPara("FK_MapData", _this.flowData.Sys_MapData[0].No);
        htmlContent = handler.DoMethodReturnString("Designer_Init");
    } else {
        htmlContent = htmlobj.responseText;
        if (htmlContent == null && htmlContent == "" && htmlContent == undefined) {
            htmlContent = "";
        }
   }
    if (htmlContent == "") {
        alert("开发者设计的表单内容丢失，请联系管理员");
        return;
    }
    //不同的组件调用改页面显示的路径不相同
    if (currentURL.toLowerCase().indexOf("frmgener") != -1 || currentURL.toLowerCase().indexOf("mybill") != -1 ||currentURL.toLowerCase().indexOf("mydict") != -1)
        htmlContent = htmlContent.replace(new RegExp("../../../", 'gm'), "../../");
    else
        htmlContent = htmlContent.replace(new RegExp("../../../", 'gm'), "../");

       // htmlContent = htmlContent.replace(new RegExp('class="form-control Wdate"', 'gm'), '@click="showDataPick(this)" class="Wdate"');

    _this.developHtml=htmlContent;
        
}

/**
 * 解析开发者表单
 * @param {跳转页面传的参数} params 
 * @param {表单数据} dataSet
 */
function ParseDevelopFrm(params,dataSet){
    //1.解析开发者表单的内容
    //1.1 所有的表单移除form-control的属性
    $("input").removeClass("form-control");
    //1.2.修改表单中的input 元素样式
    $("input[type=text]").removeClass("form-control").addClass("el-input__inner");

    //1.3 下拉框的样式
    $("select").removeClass("form-control").addClass("el-input__inner").wrap('<div class="el-input el-input--suffix is-focus"></div>');
    $("select").after('<span class="el-input__suffix"><span class="el-input__suffix-inner"><i class="el-select__caret el-input__icon el-icon-arrow-up"></i></span></span>')

   
    //1.5 移除从表，附件的解析，暂时在MyFlowGener.vue中表单字段显示
    $("img[leipiplugins='dtl']").remove();
    $("img[leipiplugins='ath']").remove();

    
    //2.加载隐藏字段，
    var mapAttrs = dataSet.Sys_MapAttr;
    var html = "";
    for (var i = 0; i < mapAttrs.length; i++) {
        var mapAttr = mapAttrs[i];

        //判断是否必填
        //var mustInput = mapAttr.UIIsInput
        //设置字段的样式属性
        $('#TB_' + mapAttr.KeyOfEn).addClass(mapAttr.CSS);
        $('#RB_' + mapAttr.KeyOfEn).addClass(mapAttr.CSS);
        $('#DDL_' + mapAttr.KeyOfEn).addClass(mapAttr.CSS);
        $('#CB_' + mapAttr.KeyOfEn).addClass(mapAttr.CSS);

        //如果是时间控件
        var frmDate="";
        var dateFmt="";
        if (mapAttr.MyDataType == 6 && (mapAttr.UIIsEnable != 0 && params.IsReadonly != "1")) {
            frmDate = mapAttr.IsSupperText;
            dateFmt = '';
            if (frmDate == 0) {
                dateFmt = "yyyy-mm-dd";
            } else if (frmDate == 3) {
                dateFmt = "yyyy-mm";
            } else if (frmDate == 6) {
                dateFmt = "mm-dd";
            }
            
            //修改时间控件的格式
            var divData=$('<div class="el-date-editor el-input el-input--prefix el-input--suffix el-date-editor--date date datetimePicker"></div>');
            $("#TB_"+mapAttr.KeyOfEn).wrap(divData);
            $("#TB_"+mapAttr.KeyOfEn).after('<span class="el-input__prefix"><i class="el-input__icon el-icon-date"></i></span>');
            //绑定click事件
            $('#TB_' + mapAttr.KeyOfEn).datetimepicker({
                format: dateFmt,
                autoclose: true,
                todayBtn: true,
                todayHighlight: true,
                showMeridian: true,
                pickerPosition: "bottom-left",
                language: 'zh-CN',//中文，需要引用zh-CN.js包
                startView: 2,//月视图
                minView: 2//日期时间选择器所能够提供的最精确的时间选择视图
            });

            

        }
        if (mapAttr.MyDataType == 7 && (mapAttr.UIIsEnable != 0 && params.IsReadonly != "1")) {
            frmDate = mapAttr.IsSupperText;
            dateFmt = '';
            if (frmDate == 1) {
                dateFmt = "yyyy-mm-dd hh:ii";
            } else if (frmDate == 2) {
                dateFmt = "yyyy-mm-dd hh:ii:ss";
            } else if (frmDate == 4) {
                dateFmt = "hh:ii";
            } else if (frmDate == 5) {
                dateFmt = "hh:ii:ss";
            } 
            
            $("#TB_"+mapAttr.KeyOfEn).wrap('<div class="el-date-editor el-input el-input--prefix el-input--suffix el-date-editor--date"></div>');
            $("#TB_"+mapAttr.KeyOfEn).after('<span class="el-input__prefix"><i class="el-input__icon el-icon-time"></i></span>');
            //绑定click事件
            $('#TB_' + mapAttr.KeyOfEn).datetimepicker({
                format: dateFmt,
                autoclose: true,
                todayBtn: true,
                todayHighlight: true,
                showMeridian: true,
                pickerPosition: "bottom-left",
                language: 'zh-CN',//中文，需要引用zh-CN.js包
                
            });

        }

        //外部数据源、外键的选择列表
        if ((mapAttr.LGType == "0" && mapAttr.MyDataType == "1" && mapAttr.UIContralType == 1)
            || (mapAttr.LGType == "2" && mapAttr.MyDataType == "1")) {
            var _html = InitDDLOperation(dataSet, mapAttr, null);
            $("#DDL_" + mapAttr.KeyOfEn).empty();
            $("#DDL_" + mapAttr.KeyOfEn).append(_html);

        }
        

        //为复选框高级设置绑定事件
        if (mapAttr.MyDataType == 4 && mapAttr.AtPara.indexOf('@IsEnableJS=1') >= 0) {
            $("#CB_" + mapAttr.KeyOfEn).attr("onchange", "clickEnable(this, \"" + mapAttr.FK_MapData + "\",\"" + mapAttr.KeyOfEn + "\",\"" + mapAttr.AtPara + "\",8)");
        }
            //为单选按钮高级设置绑定事件
        if (mapAttr.MyDataType == 2 && mapAttr.LGType == 1) {
            if (mapAttr.AtPara && mapAttr.AtPara.indexOf('@IsEnableJS=1') >= 0) {
                if (mapAttr.UIContralType == 1)//枚举下拉框
                    $("#CB_" + mapAttr.KeyOfEn).attr("onchange", "changeEnable(this,\"" + mapAttr.FK_MapData + "\",\"" + mapAttr.KeyOfEn + "\",\"" + mapAttr.AtPara + "\",8)");
                if (mapAttr.UIContralType == 3) { //枚举单选
                    var inputs =$("input[name='RB_"+mapAttr.KeyOfEn+"']");
                    $.each(inputs, function (i, target) {
                        $(target).attr("onchange","clickEnable( this ,\"" + mapAttr.FK_MapData + "\",\"" + mapAttr.KeyOfEn + "\",\"" + mapAttr.AtPara + "\",8)");
                    });
                    
                }
            }
        } 

        if (mapAttr.MyDataType == 1) {
            if (mapAttr.UIContralType == 8)//手写签字版
            {
                var element = $("#Img" + mapAttr.KeyOfEn);
                var defValue = ConvertDefVal(dataSet, mapAttr.DefVal, mapAttr.KeyOfEn);
                var ondblclick = ""
                if (mapAttr.UIIsEnable == 1) {
                    ondblclick = " ondblclick='figure_Develop_HandWrite(\"" + mapAttr.KeyOfEn + "\",\"" + defValue + "\")'";
                }

                html = "<input maxlength=" + mapAttr.MaxLen + "  id='TB_" + mapAttr.KeyOfEn + "'  name='TB_" + mapAttr.KeyOfEn + "'  value='" + defValue + "' type=hidden />";
                var eleHtml = "";
                if (currentURL.indexOf("FrmGener.htm") != -1 || currentURL.indexOf("MyBill.htm") != -1 || currentURL.indexOf("MyDict.htm") != -1)
                    eleHtml = "<img src='" + defValue + "' " + ondblclick + " onerror=\"this.src='../../DataUser/Siganture/UnName.jpg'\"  style='border:0px;width:100px;height:30px;' id='Img" + mapAttr.KeyOfEn + "' />" + html;
                else
                    eleHtml = "<img src='" + defValue + "' " + ondblclick + " onerror=\"this.src='../DataUser/Siganture/UnName.jpg'\"  style='border:0px;width:100px;height:30px;' id='Img" + mapAttr.KeyOfEn + "' />" + html;
                
                element.after(eleHtml);
                element.remove(); //移除Imge节点
            }
            if (mapAttr.UIContralType == 4)//地图
            {
                var obj = $("#TB_" + mapAttr.KeyOfEn);
                //获取兄弟节点
                $(obj.prev()).attr("onclick", "figure_Template_Map('" + mapAttr.KeyOfEn + "','" + mapAttr.UIIsEnable + "')");
            }
            if (mapAttr.UIContralType == 101)//评分
            {
                var scores = $(".simplestar");//获取评分的类
                $.each(scores, function () {
                    $.each($(this).children("Img"), function () {
                        if (currentURL.indexOf("FrmGener.htm") != -1 || currentURL.indexOf("MyBill.htm") != -1 || currentURL.indexOf("MyDict.htm") != -1)
                            $(this).attr("src", $(this).attr("src").replace("../../", "../"));
                        else
                            $(this).attr("src", $(this).attr("src").replace("../../", "./"));
                    });
                });

            }
        }
    }



    
}

//双击签名
/*function figure_Template_Siganture(SigantureID, val, type,params) {

    //先判断，是否存在签名图片
    var handler = new HttpHandler("BP.WF.HttpHandler.WF");
    handler.AddPara('no', val);
    var data = handler.DoMethodReturnString("HasSealPic");

    //如果不存在，就显示当前人的姓名
    if (data.length > 0 && type == 0) {
        $("#TB_" + SigantureID).before(data);
        var obj = document.getElementById("Img" + SigantureID);
        var impParent = obj.parentNode; //获取img的父对象
        impParent.removeChild(obj);
    }
    else {
        var src = UserICon + oliId + UserIConExt;    //新图片地址
        document.getElementById("Img" + SigantureID).src = src;
    }
    isSigantureChecked = true;

    var sealData = new Entities("BP.Tools.WFSealDatas");
    sealData.Retrieve("OID",params.WorkID, "FK_Node", params.FK_Node, "SealData", params.UserNo);
    if (sealData.length > 0) {
        return;
    }
    else {
        sealData = new Entity("BP.Tools.WFSealData");
        sealData.MyPK = params.WorkID+ "_" + params.FK_Node + "_" + val;
        sealData.OID = params.WorkID;
        sealData.FK_Node =params.FK_Node;
        sealData.SealData = val;
        sealData.Insert();
    }

}

//签字板
function figure_Template_HandWrite(HandWriteID,params) {
    var url = "CCForm/HandWriting.htm?WorkID=" + params.WorkID + "&FK_Node=" + params.FK_Node + "&KeyOfEn=" + HandWriteID;
    OpenEasyUiDialogExt(url, '签字板', 400, 300, false);
}
//地图
function figure_Template_Map(MapID, UIIsEnable,mainTable,params) {
    var AtPara = "";
    //通过MAINTABLE返回的参数
    for (var ele in mainTable) {
        if (ele == "AtPara" && mainTable != '') {
            AtPara = mainTable[ele];
            break;
        }
    }

    var url = "CCForm/Map.htm?WorkID=" + params.WorkID + "&FK_Node=" + params.FK_Node + "&KeyOfEn=" + MapID + "&UIIsEnable=" + UIIsEnable + "&Paras=" + AtPara;
    OpenBootStrapModal(url, "eudlgframe", "地图", 800, 500, null, false, function () { }, null, function () {

    });
}
function setHandWriteSrc(HandWriteID, imagePath) {
    imagePath = "../" + imagePath.substring(imagePath.indexOf("DataUser"));
    document.getElementById("Img" + HandWriteID).src = "";
    $("#Img" + HandWriteID).attr("src", imagePath);
    $("#TB_" + HandWriteID).val(imagePath);
    $('#eudlg').dialog('close');
}*/

export{
    GenerDevelopFrm,
    ParseDevelopFrm
}