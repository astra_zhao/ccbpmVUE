import $ from 'jquery';
import { HttpHandler,Entity,DBAccess} from './Gener.js';
import {GenerBindDDL,DealExp,GetLocalWFPreHref} from './tools.js';
// ********************** 根据关键字动态查询. ******************************** //
var oldValue = "";
var oid;
var highlightindex = -1;


/**
 * 文本自动填充
 * @param {*} sender 
 * @param {*} selectVal 
 * @param {*} tbid 
 * @param {*} fk_mapExt 
 * @param {*} TBModel 
 */
window.DoAnscToFillDiv =function(sender, selectVal, tbid, fk_mapExt,TBModel) {
    var mapExt = new Entity("BP.Sys.MapExt", fk_mapExt);
    var myEvent = window.event || arguments[0];
    var myKeyCode = myEvent.keyCode;
    // 获得ID为divinfo里面的DIV对象 .  
    var autoNodes = $("#divinfo").children("div");
    if (myKeyCode == 38) {
        if (highlightindex != -1) {
            autoNodes.eq(highlightindex).css("background-color", "white");
            autoNodes.eq(highlightindex).css("color", "Black");
            if (highlightindex == 0) {
                highlightindex = autoNodes.length - 1;
            }
            else {
                highlightindex--;
            }
        }
        else {
            highlightindex = autoNodes.length - 1;
        }
        autoNodes.eq(highlightindex).css("background-color", "rgb(40, 132, 250)");
        autoNodes.eq(highlightindex).css("color", "white");
    }
    else if (myKeyCode == 40) {
        if (highlightindex != -1) {
            autoNodes.eq(highlightindex).css("background-color", "white");
            autoNodes.eq(highlightindex).css("color", "black");
            highlightindex++;
        }
        else {
            highlightindex++;
        }

        if (highlightindex == autoNodes.length) {
            autoNodes.eq(autoNodes.length).css("background-color", "white");
            autoNodes.eq(autoNodes.length).css("color", "black");
            highlightindex = 0;
        }
        autoNodes.eq(highlightindex).css("background-color", "rgb(40, 132, 250)");
        autoNodes.eq(highlightindex).css("color", "white");
    }
    else if (myKeyCode == 13) {
        if (highlightindex != -1) {

            //获得选中的那个的文本值
            var textInputText = autoNodes.eq(highlightindex).text();
            var strs = textInputText.split('|');
            autoNodes.eq(highlightindex).css("background-color", "white");
            $("#" + tbid).val(strs[0]);
            $("#divinfo").hide();
            oldValue = strs[0];

            // 填充.
            FullIt(oldValue, mapExt.MyPK, tbid);
            highlightindex = -1;
        }
    }
    else {
        if (selectVal != oldValue) {
            $("#divinfo").empty();
            //获得对象.
           
            var dataObj = GenerDB(mapExt.Tag4, selectVal, mapExt.DBType);
            if ($.isEmptyObject(dataObj)) {
                $("#divinfo").hide();
                return;
            }

            //简洁模式
            if (TBModel == "Simple"){
                var div = $('<div role="region" class="el-autocomplete-suggestion el-popper" style="transform-origin: center top 0px; z-index: 2009; width: 180px; position: absolute; top: 195px; left: 780px;" x-placement="bottom-start">');
                var scrolldiv =$('<div class="el-scrollbar"></div');
                var complete = $('<div class="el-autocomplete-suggestion__wrap el-scrollbar__wrap" style="margin-bottom: -21px; margin-right: -21px;"><ul class="el-scrollbar__view el-autocomplete-suggestion__list" role="listbox" id="el-autocomplete-5792"></ul></div>');
                scrolldiv.append(complete).append('<div class="el-scrollbar__bar is-horizontal"><div class="el-scrollbar__thumb" style="transform: translateX(0%);"></div></div>')
                .append('<div class="el-scrollbar__bar is-vertical"><div class="el-scrollbar__thumb" style="transform: translateY(0%); height: 15.678%;"></div></div>');

                div.append(scrolldiv).append('<div x-arrow="" class="popper__arrow" style="left: 35px;"></div>');
                
                
                $.each(dataObj, function (idx, item) {

                    var no = item.No;
                    if (no == undefined)
                        no = item.NO;

                    var name = item.Name;
                    if (name == undefined)
                        name = item.NAME;
                    
                    div.append("<li  name='" + idx + "' onmouseover='MyOver(this)' onmouseout='MyOut(this)' onclick=\"ItemClick('" + no + "','" + tbid + "','" + fk_mapExt + "');\" value='" + no + "'>"+ no + '|' + name +"</li>");

                });
                $("#divinfo").append(div);
                $("#divinfo").show();

            }

            //表格模式
            if(TBModel=="Table")
                showDataGrid(tbid, dataObj, mapExt); 

            oldValue = selectVal;

        }
    }
}

//文本自动填充 表格模式
function showDataGrid(tbid, dataObj,mapExt) {
    var columns = mapExt.Tag3;
    //var div = $('<div role="tooltip" id="el-popover-8642" aria-hidden="false" class="el-popover el-popper" tabindex="0" style="width: 400px; transform-origin: left center 0px; z-index: 2024; position: absolute; top: 38px; left: 776px;" x-placement="right">');
    //var tablediv = $('<div class="el-table el-table--fit el-table--scrollable-x el-table--enable-row-hover el-table--enable-row-transition">');
    //var thdiv=$('<div class="el-table__header-wrapper"><table cellspacing="0" cellpadding="0" border="0" class="el-table__header" style="width: 550px;"></table></div>');
    //var tddiv=$('<div class="el-table__body-wrapper is-scrolling-left"><table cellspacing="0" cellpadding="0" border="0" class="el-table__body" style="width: 550px;"></table></div>');
    
    $("#divinfo").append(" <table id='viewGrid'></table>");
    //取消DIV的宽度
    document.getElementById("divinfo").style.width="";

    var searchTableColumns = [{
         //title: 'Number',//标题  可不加  
        formatter: function (value, row, index) {  
            return index+1;  
        }  

    }];
            
    //显示列的中文名称.
    if (typeof columns == "string") {
        $.each(columns.split(","), function (i, o) {
            var exp = o.split("=");
            var field;
            var title;
            if (exp.length == 1) {
                field = title = exp[0];
            } else if (exp.length == 2) {
                field = exp[0];
                title = exp[1];
            }
            if (!isLegalName(field)) {
                return true;
            }
            searchTableColumns.push({
                field: field,
                title: title

            });
        });
       
        var options = {
            striped: true,
            cache: false,
            showHeader:true,
            sortOrder: "asc",
            strictSearch: true,
            minimumCountColumns: 2,
            highlightSelected: true,
            clickToSelect: true,
            singleSelect: true,
            sortable: false,
            cardView: false,
            detailView: false,
            uniqueId: "No",
            columns: searchTableColumns,
            rowStyle: function () {
                var style = {};
                style = 'active';
                return { classes: style };
            }
        };
        options.onClickRow = function (row) {
            $("#divinfo").empty();
            $("#divinfo").css("display", "none");
            highlightindex = -1;
            $("#" + tbid).val(row.No);

            FullIt(row.No, mapExt.MyPK,tbid);
           
        };
        $('#viewGrid').bootstrapTable(options);
        $('#viewGrid').bootstrapTable("load", dataObj);
    }
   
  
}

function isLegalName(name) {
    if (!name) {
        return false;
    }
    //warning ^[a-zA-Z\$_][a-zA-Z\d\$_]*$ 修改待定
    return name.match(/^[a-zA-Z$_][a-zA-Z\d$_]*$/);
}

//根据Name设置元素的值  分为 tb,ddl,rd
function SetEleValByName(eleName, val) {
    var ele = $('[name$=_' + eleName + ']');
    if (ele != undefined && ele.length > 0) {
        switch (ele[0].tagName.toUpperCase()) {
            case "INPUT":
                switch (ele[0].type.toUpperCase()) {
                    case "CHECKBOX": //复选框  0:false  1:true
                        val.indexOf('1') >= 0 ? $(ele).attr('checked', true) : $(ele).attr('checked', false);
                        break;
                    case "TEXT": //文本框
                        $(ele).val(val);
                        break;
                    case "RADIO": //单选钮
                        $(ele).attr('checked', false);
                        $('[name=RB_' + eleName + '][value=' + val + ']').attr('checked', true);
                        break;
                    case "HIDDEN":
                        $(ele).val(val);
                        break;
                }
                break;
            //下拉框   
            case "SELECT":
                $(ele).val(val);
                break;
            //文本区域   
            case "TEXTAREA":
                $(ele).val(val);
                break;
        }
    }
}



/*  ReturnValTBFullCtrl */
function ReturnValTBFullCtrl(ctrl, fk_mapExt) {
    var wfPreHref = GetLocalWFPreHref();
    var url = wfPreHref + '/WF/CCForm/FrmReturnValTBFullCtrl.aspx?CtrlVal=' + ctrl.value + '&FK_MapExt=' + fk_mapExt;
    var v = window.showModalDialog(url, 'wd', 'scrollbars=yes;resizable=yes;center=yes;minimize:yes;maximize:yes;dialogHeight: 650px; dialogWidth: 850px; dialogTop: 100px; dialogLeft: 150px;');
    if (v == null || v == '' || v == 'NaN') {
        return;
    }
    ctrl.value = v;
    // 填充.
    FullIt(oldValue, ctrl.id, fk_mapExt);
    return;
}




/* 自动填充 */
window.DDLFullCtrl=function (selectVal, ddlChild, fk_mapExt) {

    FullIt(selectVal, fk_mapExt,ddlChild);
}

/* 级联下拉框  param 传到后台的一些参数  例如从表的行数据 主表的字段值 如果param参数在，就不去页面中取KVS 了，PARAM 就是*/
window.DDLAnsc = function(selectVal, ddlChild, fk_mapExt, param) {
    var chg;
    //1.初始值为空或者NULL时，相关联的字段没有数据显示
    if (selectVal == null || selectVal == "") {
        $("#" + ddlChild).empty();
        //无数据返回时，提示显示无数据，并将与此关联的下级下拉框也处理一遍，edited by liuxc,2015-10-22
        $("#" + ddlChild).append("<option value='' selected='selected' ></option");
        chg = $("#" + ddlChild).attr("onchange");

        $("#" + ddlChild).change();

        return;
    }
    if (selectVal == "all") {
        $("#" + ddlChild).empty();
        //无数据返回时，提示显示无数据，并将与此关联的下级下拉框也处理一遍，edited by liuxc,2015-10-22
        $("#" + ddlChild).append("<option value='all' selected='selected' >全部</option");
        chg = $("#" + ddlChild).attr("onchange");

        $("#" + ddlChild).change();

        return;
    }

    GenerPageKVs();

    var mapExt = new Entity("BP.Sys.MapExt", fk_mapExt);

    //处理参数问题
    if (param != undefined) {
        kvs = '';
    }
    var dbSrc = mapExt.Doc;
    if (param != undefined) {
        for (var pro in param) {
            if (pro == 'DoType')
                continue;
            dbSrc = dbSrc.replace("@" + pro, param[pro]);
        }
    }

    var dataObj = GenerDB(dbSrc, selectVal, mapExt.DBType); //获得数据源.

    // 这里要设置一下获取的外部数据.
    // 获取原来选择值.
    var oldVal = null;
    var ddl = document.getElementById(ddlChild);

    if (ddl == null) {
        alert(ddlChild + "丢失,或者该字段被删除.");
        return;
    }


    var mylen = ddl.options.length - 1;
    while (mylen >= 0) {
        if (ddl.options[mylen].selected) {
            oldVal = ddl.options[mylen].value;
        }
        mylen--;
    }

    //清空级联字段
    $("#" + ddlChild).empty();

    //查询数据为空时为级联字段赋值
    if (dataObj == null || dataObj.length == 0) {
        //无数据返回时，提示显示无数据，并将与此关联的下级下拉框也处理一遍，edited by liuxc,2015-10-22
        $("#" + ddlChild).append("<option value='' selected='selected' ></option");
        chg = $("#" + ddlChild).attr("onchange");

        if (typeof chg == "function") {
            $("#" + ddlChild).change();
        }
        return;
    }

    //不为空的时候赋值
    $.each(dataObj, function (idx, item) {

        //  alert(item[idx][1]);
        //console.log(item);
        //return;

        var no = item.No;
        if (no == undefined)
            no = item.NO;

        var name = item.Name;
        if (name == undefined)
            name = item.NAME;

        $("#" + ddlChild).append("<option value='" + no + "'>" + name + "</option");

    });

    var isInIt = false;
    mylen = ddl.options.length - 1;
    while (mylen >= 0) {
        if (ddl.options[mylen].value == oldVal) {
            ddl.options[mylen].selected = true;
            isInIt = true;
            break;
        }
        mylen--;
    }
    if (isInIt == false) {
        //此处修改，去掉直接选中上次的结果，避免错误数据的产生，edited by liuxc,2015-10-22
        //$("#" + ddlChild).prepend("<option value='' selected='selected' >*请选择</option");
        $("#" + ddlChild).val('');
        //增加默认选择第一条数据
        ddl.options[0].selected = true;
        chg = $("#" + ddlChild).attr("onchange");
        $("#" + ddlChild).change();
       
    }
}

window.ItemClick=function(val, tbid, fk_mapExt) {

    $("#divinfo").empty();
    $("#divinfo").css("display", "none");
    highlightindex = -1;
    oldValue = val;

    $("#" + tbid).val(oldValue);

    // 填充.
    FullIt(oldValue,fk_mapExt,tbid);
}

window.MyOver =function(sender) {
    if (highlightindex != -1) {
        $("#divinfo").children("div").eq(highlightindex).css("background-color", "white");
    }

    highlightindex = $(sender).attr("name");
    $(sender).css("background-color", "rgb(40, 132, 250)");
    $(sender).css("color", "white");
}

window.MyOut = function(sender) {
    $(sender).css("background-color", "white");
    $(sender).css("color", "black");
}

function hiddiv() {
    $("#divinfo").css("display", "none");
}

// 获取参数.
function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
        return decodeURI(r[2]);
    return null;
}
//通过正则表达式检测
function CheckInput(oInput, filter) {
    var re = filter;
    if (typeof (filter) == "string") {
        re = new RegExp(filter);
    }
    return re.test(oInput);
}
//正则表达式检查
window.CheckRegInput=function(oInput, filter, tipInfo) {
    var mapExt = $('#' + oInput).data();
    filter = mapExt.Doc.replace(/【/g, '[').replace(/】/g, ']').replace(/（/g, '(').replace(/）/g, ')').replace(/｛/g, '{').replace(/｝/g, '}');
    var oInputVal = $("[name=" + oInput + ']').val();
    var result = true;
    var errorId="";
    if (oInput != '') {
        var re = filter;
        if (typeof (filter) == "string") {
            if (filter.indexOf('/') == 0) {
                filter = filter.substr(1, filter.length - 2);
            }

            re = new RegExp(filter);
        } else {
            re = filter;
        }
        result = re.test(oInputVal);
    }
    if (!result) {
        $("[name=" + oInput + ']').addClass('errorInput');
        errorId = oInput+"error";
        if($("#"+errorId).length == 0){
           var span = $("<div id='"+errorId+"' style='color:red;line-height: 1;padding: 4px 0px 0px 20px;font-size: 13px;text-align:left'></div>");
           $("[name=" + oInput + ']').parent().append(span);
        }
        $("#"+errorId).html(tipInfo);

    } else {
        $("[name=" + oInput + ']').removeClass('errorInput');
        errorId = oInput+"error";
        if ($("#" + errorId).length != 0)
            $("#" + errorId).remove();
    }
    return result;
}
//输入检查
function txtTest_Onkeyup(ele, filter, message) {
    if (ele == null) return;
    var re = filter;
    if (typeof (filter) == "string") {
        re = new RegExp(filter);
    }
    var format = re.test(ele.value);
    if (!format) {
        ele.value = "";
        alert(message);
    }
}



/**
* 填充其他控件
* @param selectVal 选中的值
* @param refPK mapExt关联的主键
* @param elementId 元素ID
*/
function FullIt(selectVal, refPK, elementId) {
    if (oid == null)
        oid = GetQueryString('OID');

    if (oid == null)
        oid = GetQueryString('WorkID');

    if (oid == null)
        oid = GetQueryString("RefPKVal");

    if (oid == null) {
        oid = 0;
        return;
    }
    var mypk ="";
    if (refPK.indexOf("FullData") != -1)
        mypk = refPK;
    else
        mypk = refPK + "_FullData";

    //获得对象.
    var mapExt = new Entity("BP.Sys.MapExt");
    mapExt.SetPKVal(mypk);
    var i = mapExt.RetrieveFromDBSources();

    //没有填充其他控件
    if (i == 0)
        return;
   
    //生成关键字.
    GenerPageKVs();

    //执行填充主表的控件.
    FullCtrl(selectVal, elementId, mapExt);

    //执行个性化填充下拉框，比如填充ddl下拉框的范围.
    FullCtrlDDL(selectVal, elementId, mapExt);

    //执行填充从表.
    FullDtl(selectVal, mapExt);

    //执行确定后执行的JS
    var backFunc = mapExt.Tag2;
    if (backFunc != null && backFunc != "" && backFunc != undefined)
        DBAccess.RunFunctionReturnStr(DealSQL(backFunc, selectVal));
}


/**
*  填充主表数据信息.
*/
function FullCtrl(selectVal, ctrlIdBefore, mapExt) {
    var dbSrc = mapExt.Doc;
    if (dbSrc == null || dbSrc == "") {
        alert("没有配置填充主表的信息");
        return;
    }
    var dataObj = GenerDB(dbSrc, selectVal, mapExt.DBType);

    TableFullCtrl(dataObj, ctrlIdBefore);
}

function TableFullCtrl(dataObj, ctrlIdBefore) {

    if ($.isEmptyObject(dataObj)) {
        // alert('系统错误不应该查询不到数据:'+dbSrc);
        return;
    }

    var data = dataObj[0]; //获得这一行数据.


    //针对主表或者从表的文本框自动填充功能，需要确定填充的ID
    var beforeID = null;
    var endId = null;

    // 根据ddl 与 tb 不同。
    if (ctrlIdBefore.indexOf('DDL_') > 1) {
        beforeID = ctrlIdBefore.substring(0, ctrlIdBefore.indexOf('DDL_'));
        endId = ctrlIdBefore.substring(ctrlIdBefore.lastIndexOf('_'));
    } else {
        beforeID = ctrlIdBefore.substring(0, ctrlIdBefore.indexOf('TB_'));
        endId = ctrlIdBefore.substring(ctrlIdBefore.lastIndexOf('_'));
    }

    //遍历属性，给属性赋值.
    var valID;
    var tbs;
    var selects;
    for (var key in data) {

        var val = data[key];
        valID = $("#" + beforeID + "TB_" + key);
        if (valID.length == 1) {
            valID.val(val);
            continue;
        }
        valID = $("#" + beforeID + "TB_" + key + endId);
        if (valID.length == 1) {
            valID.val(val);
            continue;
        }

        valID = $("#" + beforeID + "DDL_" + key)
        if (valID.length == 1) {
            valID.val(val);
            continue;
        }
        valID = $("#" + beforeID + "DDL_" + key + endId);
        if (valID.length == 1) {
            valID.val(val);
            continue;
        }

        valID = $("#" + beforeID + 'CB_' + key);
        if (valID.length == 1) {
            if (val == '1') {
                valID.attr("checked", true);
            } else {
                valID.attr("checked", false);

            }
            continue;
        }
        valID = $("#" + beforeID + 'CB_' + key + endId);
        if (valID.length == 1) {
            if (val == '1') {
                valID.attr("checked", true);
            } else {
                valID.attr("checked", false);

            }
            continue;
        }

        //获取表单中所有的字段
        if (valID.length == 0) {
            if (tbs == undefined || tbs=="") {
                if (endId != "") {
                    //获取所在的行
                    tbs = $("#" + ctrlIdBefore).parent().parent().find("input")
                }
                else
                   tbs = $('input');
            }
            if (selects == undefined || selects == "") {
                if (endId != "") {
                    //获取所在的行
                    selects = $("#" + ctrlIdBefore).parent().parent().find("select")
                }
                else
                    selects = $('select');
            }
            
            $.each(tbs, function (i, tb) {
                var name = $(tb).attr("id");
                if (name == null || name == undefined)
                    return false;
                if (name.toUpperCase().indexOf(key) >= 0) {
                    if (name.indexOf("TB_") == 0)
                        $("#" + name).val(val);
                    if (name.indexOf("CB_")) {
                        if (val == '1') {
                            $("#" + name).attr("checked", true);
                        } else {
                            $("#" + name).attr("checked", false);

                        }
                    }
                    return false;
                }
            });
            $.each(selects, function (i, select) {
                var name = $(select).attr("id");
                if (name.toUpperCase().indexOf(key) >= 0) {
                    $("#" + name).val(val);
                    return false;
                }
            });
        }
    }
}

/**填充下拉框信息**/
function FullCtrlDDL(selectVal, ctrlID , mapExt) {

    var doc = mapExt.Tag;
    if (doc == "" || doc == null)
        return;
   
    var dbSrcs = doc.split('$'); //获得集合.
    for (var i = 0; i < dbSrcs.length; i++) {

        var dbSrc = dbSrcs[i];
        if (dbSrc == "" || dbSrc.length == 0)
            continue;
        ctrlID = dbSrc.substring(0, dbSrc.indexOf(':'));
        var src = dbSrc.substring(dbSrc.indexOf(':') + 1);

        var db = GenerDB(src, selectVal, mapExt.DBType); //获得数据源.

        //重新绑定下拉框.
        GenerBindDDL("DDL_" + ctrlID, db);
    }
}
//填充明细.
function FullDtl(selectVal, mapExt) {
    if (mapExt.Tag1 == "" || mapExt.Tag1 == null)
        return;

    var dbType = mapExt.DBType;
    var dbSrc = mapExt.Tag1;
    var dataObj;

    if (dbType == 1) {

        dbSrc = DealSQL(DealExp(dbSrc), selectVal, kvs);
        dataObj = DBAccess.RunDBSrc(dbSrc, 1);

        //JQuery 获取数据源
    } else if (dbType == 2) {
        dataObj = DBAccess.RunDBSrc(dbSrc, 2);
    } else {
        var handler = new HttpHandler("BP.WF.HttpHandler.WF_CCForm");
        handler.AddPara("Key", selectVal);
        handler.AddPara("FK_MapExt", mapExt.MyPK);
        handler.AddPara("KVs", kvs);
        handler.AddPara("DoTypeExt", "ReqDtlFullList");
        handler.AddPara("OID", oid);
        var data = handler.DoMethodReturnString("HandlerMapExt");
        if (data == "")
            return;

        if (data.indexOf('err@') == 0) {
            alert(data);
            return;
        }
        dataObj = eval("(" + data + ")"); //转换为json对象 	
    }

    for (var i in dataObj.Head) {
        if (typeof (i) == "function")
            continue;

        for (var k in dataObj.Head[i]) {
            var fullDtl = dataObj.Head[i][k];
            //  alert('您确定要填充从表吗?，里面的数据将要被删除。' + key + ' ID= ' + fullDtl);
            var frm = document.getElementById('Dtl_' + fullDtl);
            var src = frm.src;
            var idx = src.indexOf("&Key");
            if (idx == -1)
                src = src + '&Key=' + selectVal + '&FK_MapExt=' + mapExt.MyPK;
            else
                src = src.substring(0, idx) + '&ss=d&Key=' + selectVal + '&FK_MapExt=' + mapExt.MyPK;
            frm.src = src;
        }
    }
}
var kvs = "";
function GenerPageKVs() {
    var ddls = null;
    ddls = parent.document.getElementsByTagName("select");
    
    var id="";
    var myid="";
    for (var i = 0; i < ddls.length; i++) {
        id = ddls[i].name;

        if (id.indexOf('DDL_') == -1) {
            continue;
        }
        myid = id.substring(id.indexOf('DDL_') + 4);
        kvs += '~' + myid + '=' + ddls[i].value;
    }

    ddls = document.getElementsByTagName("select");
    for (i = 0; i < ddls.length; i++) {
        id = ddls[i].name;

        if (id.indexOf('DDL_') == -1) {
            continue;
        }
        myid = id.substring(id.indexOf('DDL_') + 4);
        kvs += '~' + myid + '=' + ddls[i].value;
    }
    return kvs;
}

/**处理SQL语句/函数/**/
function GenerDB(dbSrc, selectVal, dbType) {


    //处理sql，url参数.
    dbSrc = dbSrc.replace(/~/g, "'");

    dbSrc = dbSrc.replace('@Key', selectVal);

    dbSrc = DealExp(dbSrc);
    dbSrc = DealSQL(dbSrc, selectVal, kvs);

    //获取数据源.
    var dataObj = DBAccess.RunDBSrc(dbSrc, dbType);
    return dataObj;
}

function DealSQL(dbSrc, key, kvs) {

    if (dbSrc.indexOf('@') == -1)
        return dbSrc;

    dbSrc = dbSrc.replace(/~/g, "'");

    dbSrc = dbSrc.replace(/@Key/g, key);
    dbSrc = dbSrc.replace(/@Val/g, key);
    dbSrc = dbSrc.replace(/\n/g, "");
    var oid = GetQueryString("OID");
    if (oid != null) {
        dbSrc = dbSrc.replace("@OID", oid);
    }

    if (kvs != null && kvs != "" && dbSrc.indexOf("@") >= 0) {

        var strs = kvs.split("[~]", -1);
        for (var i = 0; i < strs.length; i++) {
            var s = strs[i];
            if (s == null || s == "" || s.indexOf("=") == -1)
                continue;
            var mykv = s.split("[=]", -1);
            dbSrc = dbSrc.replace("@" + mykv[0], mykv[1]);
            if (dbSrc.indexOf("@") == -1)
                break;
        }
    }

    return dbSrc;
}

export{

    //DoAnscToFillDiv,
    FullIt,
    SetEleValByName,
    ReturnValTBFullCtrl,
    hiddiv,
    CheckInput,
    txtTest_Onkeyup,
    DealSQL

}


