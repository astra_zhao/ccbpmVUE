/**String类型字段的POP弹出功能 */
import $ from 'jquery';
import {Entity,Entities,DBAccess} from '../Gener.js';
import {GetPKVal} from '../tools.js';
import {FullIt,DealSQL} from '../MapExt.js';

/**
 * POP弹出模式
 * @param {扩展属性} mapExt 
 * @param {默认值} val 
 * @param {操作DOM的ID}} targetId 
 * @param {这个是针对从表使用的} index 
 * @param {获取表单的主键，住表单是WorkID，从表是OID} oid 
 * @param {*} objtr 
 */
function OpenPopModel(mapExt, val,_this,popModel, targetId, index,oid,objtr) {

    var mtagsId;
    if (targetId == null || targetId == undefined)
        targetId = "TB_" + mapExt.AttrOfOper;

    var target = $("#" + targetId);

    var width = target.outerWidth();
    var height = target.outerHeight();
    target.hide();
    var container = $("<div class='el-input__inner'></div>");
    target.after(container);
    container.width(width);
    container.height(height);
    if (index == null || index == undefined) {
        container.attr("id", mapExt.AttrOfOper + "_mtags");
        mtagsId = mapExt.AttrOfOper + "_mtags";
    }
    else {
        container.attr("id", mapExt.AttrOfOper + "_mtags_" + index);
        mtagsId = mapExt.AttrOfOper + "_mtags_" + index;
    }
    var mtags = $("#" + mtagsId);
    mtags.mtags({
        "fit": true,
        "onUnselect": function (record) {
            Delete_FrmEleDB(mapExt.AttrOfOper, oid, record.No);
            console.log("unselect: " + JSON.stringify(record));
        }
    });

    width = mapExt.W;
    height = mapExt.H;
    var title = mapExt.GetPara("Title");
    if (oid == null || oid == undefined)
        oid = GetPKVal(_this.$route.params);

    var frmEleDBs = new Entities("BP.Sys.FrmEleDBs");
    frmEleDBs.Retrieve("FK_MapData", mapExt.FK_MapData, "EleID", mapExt.AttrOfOper, "RefPKVal", oid);
    if (frmEleDBs.length == 0 && val != "")
        frmEleDBs = [{ "Tag1": "", "Tag2": val}];
    var initJsonData = [];
    $.each(frmEleDBs, function (i, o) {
        initJsonData.push({
            "No": o.Tag1,
            "Name": o.Tag2
        });
    });

    mtags.mtags("loadData", initJsonData);
    $("#" + targetId).val(mtags.mtags("getText"));
    container.on("dblclick", function () {   
            var paras={};   
            var data = "";
            if (objtr == "" || objtr == null || objtr == undefined) {
                paras["ats"]=1;
            }
            else {
                data = $(objtr).data().data;
                Object.keys(data).forEach(function (key) {
                    if (key != "OID" && key != "FID" && key != "Rec" && key != "RefPK" && key != "RDT") 
                    paras[key] = data[key];
                    
                });
            }
            _this.Poptitle = title;
            _this.popVisible = true;
            _this.popWidth = '80%';
            _this.PopOperation=popModel;
            paras["MyPK"] =  mapExt.MyPK;
            paras["FK_MapData"] =  mapExt.FK_MapData;
            paras["PKVal"] =  oid;
            paras["OID"] =  oid;
            paras["mtagsId"] =  mtagsId;
            paras["mapExt"] =  mapExt;
            paras["targetId"] =  targetId;
            _this.popParams = paras;
    });

    return;
}

/**
 * 关闭弹出框后执行的填充事件
 * @param {pop弹出窗存储的数据} popParams 
 * @param {pop弹出窗页面选择的数据} selectedRows 
 */
function ClosePopFunc(popParams,selectedRows){
    var mapExt = popParams.mapExt;
    var target = $("#"+popParams.targetId);
    var selectType = mapExt.GetPara("SelectType");
    var mtags = $("#" + popParams.mtagsId);
    mtags.mtags("loadData", selectedRows);
    target.val(mtags.mtags("getText"));
    // 单选复制当前表单
    if (selectType == "0" && selectedRows.length == 1) {
        FullIt(selectedRows[0].No, mapExt.MyPK, popParams.targetId);
    }
    var No = "";
    if (selectedRows != null && $.isArray(selectedRows))
        $.each(selectedRows, function (i, selectedRow) {
            No += selectedRow.No + ",";
        });
    //执行JS
    var backFunc = mapExt.Tag5;
    if (backFunc != null && backFunc != "" && backFunc != undefined)
        DBAccess.RunFunctionReturnStr(DealSQL(backFunc, No));
}

//删除数据.
function Delete_FrmEleDB(keyOfEn, oid, No) {
    var frmEleDB = new Entity("BP.Sys.FrmEleDB");
    frmEleDB.MyPK = keyOfEn + "_" + oid + "_" + No;
    frmEleDB.Delete();
    $("#TB_" + keyOfEn).val('');
}


export{
    ClosePopFunc,
    OpenPopModel,
}